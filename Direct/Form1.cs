﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Direct
{
    public partial class Form1 : Form
    {
        private string FileName { get; set; }
        LineHashtable LineHash { get; set; } = new LineHashtable(100);
        public Form1()
        {
            InitializeComponent();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(FormState.Add);
            addForm.ShowDialog();
            LineHash.Add(addForm.Info);
            Redraw();
        }

        private void Redraw()
        {
            List<Info> info = LineHash.GetData();
            dataGridView1.Rows.Clear();
            dataGridView1.RowCount = info.Count;
            for (int i = 0; i < info.Count; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = info[i].StudentPassport.Series;
                dataGridView1.Rows[i].Cells[1].Value = info[i].StudentPassport.Number;
                dataGridView1.Rows[i].Cells[2].Value = info[i].FIO;
                dataGridView1.Rows[i].Cells[3].Value = info[i].Address;
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.Search);
            search.ShowDialog();
            Info.Passport passport = search.Info.StudentPassport;
            Info info = LineHash.Find(passport);
            if (info != null)
            {
                AddForm edit = new AddForm(FormState.Edit, info);
                edit.ShowDialog();
                LineHash.Delete(passport);
                LineHash.Add(edit.Info);
                Redraw();
            }
            else
            {
                MessageBox.Show("Human not found");
            }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.Search);
            search.ShowDialog();
            Info.Passport passport = search.Info.StudentPassport;
            Info info = LineHash.Find(passport);
            if (info != null)
            {
               AddForm show = new AddForm(FormState.Display, info);
                show.ShowDialog(); 
            }
            else
            {
                MessageBox.Show("Human not found");
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.Delete);
            search.ShowDialog();
            Info.Passport passport = search.Info.StudentPassport;
            LineHash.Delete(passport);
            Redraw();
        } 
    }
}
