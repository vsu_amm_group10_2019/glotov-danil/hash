﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Direct
{
    public class Info
    {
        public string FIO { get; set; }
        public string Address { get; set; }
        public Passport StudentPassport { get; set; }

        public class Passport
        {
            public int Series { get; set; }
            public int Number { get; set; }
            public Passport(int series, int number)
            {
                Series = series;
                Number = number;
            }
            public Passport()
            {

            }

        };

        public bool EqualPassport(Passport pass1, Passport pass2)
        {
            bool ok;
            if ((pass1.Number == pass2.Number) & (pass1.Series == pass2.Series))
            {
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        public static int Hash(Passport passport)
        {
            int result = passport.Number * passport.Series;
            return result;
        }
    }
}
