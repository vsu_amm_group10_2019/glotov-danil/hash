﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Direct
{
    public enum FormState
    {
        Add,
        Edit,
        Search, 
        Delete,
        Display
    }
}
