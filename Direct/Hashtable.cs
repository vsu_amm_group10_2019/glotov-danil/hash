﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Direct
{
    interface IHashtable
    {
        bool Add(Info info);
        Info Find(Info.Passport passport);
        void Delete(Info.Passport passport);
        List<Info> GetData();
        void Clear();        
    }
}
